package com.example.videogame.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import com.example.videogame.R
import kotlin.random.Random

class EnemyPlane (context:Context, x: Int, y:Int, var size: Point): View(context) {

    var enemyPlane: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.bluecircle)
    var enemyPlaneWidht = x / 10f
    var enemyPlaneHeight = y / 10f
    var enemyPlaneX = 0f
    var enemyPlaneY = 0f
    var speed = 0
    var hitbox = RectF()

    //MAX SCREEN SIZE
    val screenMaxSizeX = (size.x)-100
    val screenMaxSizeY = (size.y)-100

    //HALF SCREEN SIZE
    val screenHalfX = (size.x/2f)+50
    val screenHalfY = (size.y/2f)

    init {
        enemyPlane = Bitmap.createScaledBitmap(enemyPlane, enemyPlaneWidht.toInt(), enemyPlaneHeight.toInt(),false)

        hitbox.top
        hitbox.bottom = hitbox.top + enemyPlaneHeight
        hitbox.left
        hitbox.right
    }

    fun updateEnemyPlane(){
        enemyPlaneX += speed
        enemyPlaneY += speed
    }

    fun updateSpawnPosition(list : MutableList<Spawn>): MutableList<Spawn>{
        enemyPlaneX = (screenHalfX.toInt()until screenMaxSizeX).random().toFloat()
        enemyPlaneY = (100 until screenMaxSizeY).random().toFloat()
        list.add(Spawn(enemyPlaneX,enemyPlaneY))
        return list
    }
}