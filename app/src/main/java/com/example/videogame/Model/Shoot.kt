package com.example.videogame.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.view.View
import com.example.videogame.R

class Shoot (context: Context, x: Int, y:Int, var size: Point, planeXPosition: Float,planeYPosition: Float): View(context)  {
    var shoot: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.twitter)
    val shootWidht = x / 10f
    val shootHeight = y / 10f
    var shootX = planeXPosition
    //var shootX = (size.x)/2f
    var shootY = planeYPosition
    //var shootY = (size.y)/2f
    var speed = 0

    init {
        shoot = Bitmap.createScaledBitmap(shoot, shootWidht.toInt(), shootHeight.toInt(),false)
    }

    fun updateShoot(){
        shootX += speed
        shootY += speed
    }

}