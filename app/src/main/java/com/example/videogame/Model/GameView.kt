package com.example.videogame.Model

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.graphics.RectF
import android.media.MediaPlayer
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.SurfaceView
import com.example.videogame.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class GameView(context: Context, private val size: Point, private var counter: Int) : SurfaceView(context) {

    //VARIABLES
    private var listEnemyPlanes = mutableListOf<EnemyPlane>()
    private var listShoots = mutableListOf<Shoot>()
    private var spawnPositionEnemys = mutableListOf<Spawn>()
    private var score = 0
    private var playing = true
    var totalEnemyPlanes = 3
    var shotAction = false
    var sound = Sound().soundPool

    //BACKGROUND MUSIC
    private var musicPlaying = MediaPlayer.create(context, R.raw.normalmusic)
    private var musicBoss = MediaPlayer.create(context, R.raw.bossmusic)

    //SOUND EFFECTS
    val shotMadeID = sound.load(context, R.raw.shotsound, 0) // CARREGUEM TOTS ELS ARXIUS D'AUDIO DEL NOSTRE JOC


    //FUNCTIONS TO PRINT
    private var canvas: Canvas = Canvas()
    private val paint: Paint = Paint()

    //BITMAP OF BACKGROUND
    private var background = BitmapFactory.decodeResource(resources, R.drawable.background)

    //INSTANCE PLANE'S USER
    private var plane = Plane(context, 1300,1300, size)

    //INSTANCE ENEMYS
    private var enemy = EnemyPlane(context, 1600,1600, size)
    private var bossEnemyPlane = BossEnemyPlane(context, 1800,1800, size)

    var positionXPlane = plane.planeX
    var positionYPlane = plane.planeY

    //INSTANCE SHOT
    private var shoot = Shoot(context,1000,1000,size,positionXPlane,positionYPlane)

    //BITMAP OF OBJECTS ACCEDING TO THE CLASS PROPERTY (BitmapFactory.decodeResource, to access to the image and print it)
    private var planeBitMap = plane.plane
    private var bossEnemyPlaneBitMap = bossEnemyPlane.bossEnemyPlane

    init {
        startGame()
    }

    private fun startGame(){
        for (i in 0 until totalEnemyPlanes){
            listEnemyPlanes.add(enemy)
        }
        CreateRandomPositions()
        CoroutineScope(Dispatchers.Main).launch{
            while(playing){
                draw()
                update()
                delay(5)
            }
        }
        musicPlaying?.start()
        musicPlaying?.setOnCompletionListener {
            musicPlaying?.start()
        }
    }

    private fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()
            // WE DRAW GAME'S ELEMENTS
            //SCORE
            paint.color = Color.YELLOW
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawText("Score: $score", (size.x - paint.descent()), 75f, paint)
            //BACKGROUND
            canvas.drawBitmap(background,0f,0f,paint)
            //PLAYER
            canvas.drawBitmap(planeBitMap,plane.planeX,plane.planeY,paint )
            //ENEMYS
            for (i in 0 until totalEnemyPlanes) {
                canvas.drawBitmap(listEnemyPlanes[i].enemyPlane,spawnPositionEnemys[i].x,spawnPositionEnemys[i].y,paint )
            }
            //BOSS
            if (score>12){
                for (i in 0 until listEnemyPlanes.size){
                    listEnemyPlanes[i].enemyPlaneWidht = 0f
                    listEnemyPlanes[i].enemyPlaneHeight = 0f
                }
                canvas.drawBitmap(bossEnemyPlaneBitMap,bossEnemyPlane.bossEnemyPlaneX,bossEnemyPlane.bossEnemyPlaneY,paint )
            }
            //SHOOT
            if (shotAction){
                for (i in 0 until listShoots.size) {
                    canvas.drawBitmap(listShoots[i].shoot,listShoots[i].shootX,listShoots[i].shootY,paint )
                }
            }
            holder.unlockCanvasAndPost(canvas)
        }
    }

    fun update(){

        if (score > 12){
        bossEnemyPlane.updateBossEnemyPlane()
            if (musicPlaying?.isPlaying == true) {
                musicPlaying?.pause()
                musicBoss?.start()
            }

            musicBoss?.start()
            musicPlaying?.setOnCompletionListener {
                musicBoss?.start()
            }
        }
        //USER PLANE
        plane.updatePlane()

        //ENEMYS PLANES
        for (i in 0 until totalEnemyPlanes){
            AutoMoveEnemys()
            listEnemyPlanes[i].updateEnemyPlane()
        }

        //SHOOTING
        if(shotAction){
            for (i in 0 until listShoots.size){
                listShoots[i].shootX += 20
                //listShoots[i].speed = positionXPlane.toInt()
                listShoots[i].updateShoot()
                //println(listShoots[i].speed)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            when(event.action){
                MotionEvent.ACTION_MOVE -> {
                    // Modifiquem la velocitat del aviò del usuari
                    val clickedX = event.x
                    val clickedY = event.y
                    if (clickedX > plane.planeX) {
                        plane.planeX += 3.5f
                        positionXPlane += 3.5f
                    } else {
                        plane.planeX -= 3.5f
                        positionXPlane -= 3.5f
                    }

                    if (clickedY > plane.planeY) {
                        plane.planeY += 3.5f
                        positionYPlane += 3.5f
                    } else {
                        plane.planeY -= 3.5f
                        positionYPlane -= 3.5f
                    }
                }

                MotionEvent.ACTION_DOWN -> {

                    println("hola")
                    shotAction = true
                    val shoot2 = Shoot(context,750,750,size,positionXPlane,positionYPlane)
                    listShoots.add(shoot2)
                    println(listShoots.size)
                    //Sound().playSound(shotMadeID)

                    sound.setOnLoadCompleteListener { soundPool, sampleId, status ->
                        if (status == 0) {
                            Sound().playSound(shotMadeID)
                        }
                    }
                }

                MotionEvent.ACTION_UP -> {
                    //shotAction = true
                    // Modifiquem la velocitat de la bala
                    if(event.x>shoot.shootX){
                        shoot.speed += 0
                    }
                    if (event.y>shoot.shootY){
                        shoot.speed = 0
                    }
                }

            }
        }
        return true
    }

    fun KillEnemy(enemy: EnemyPlane){
        //listEnemyPlanes.removeAt(enemy)
        listEnemyPlanes.remove(enemy)
        score++
    }

    private fun CreateRandomPositions(){
        for (i in 0 until totalEnemyPlanes){
            enemy.updateSpawnPosition(spawnPositionEnemys) //Private function from enemy class, to randomize the starting positions of the enemy planes
        }
    }

    private fun AutoMoveEnemys(){
        for (i in 0 until totalEnemyPlanes){
            spawnPositionEnemys[i].x += 0
            spawnPositionEnemys[i].y -= random() //Function to randomice the movent in y axis
        }
    }

    private fun random(): Int { //FUNCTION TO RANDOMIZE IF PLANE MOVES POSITIVE (DOWN SCREEN) OR NEGATIVE (UPPER SCREEN)
        val random = Random()
        return if (random.nextBoolean()) 2 else -2
    }
}