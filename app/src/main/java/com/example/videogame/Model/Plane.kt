package com.example.videogame.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.RectF
import android.view.View
import com.example.videogame.R

class Plane (context: Context, x: Int, y:Int, size: Point): View(context) {

    var plane: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.orangecircle)
    val planeWidht = x / 10f
    val planeHeight = y / 10f
    var planeX = 0f
    var planeY = 0f
    var speed = 0
    var hitbox = RectF()

    var ScreenLimitX = size.x
    var ScreenLimitY = size.y //1014


    init {
        plane = Bitmap.createScaledBitmap(plane, planeWidht.toInt(), planeHeight.toInt(),false)

        planeX = size.x/10f
        planeY = size.y/2f

        hitbox.top = planeY - (planeHeight/2)
        hitbox.bottom = hitbox.top + planeHeight
        hitbox.left = planeX - (planeWidht/2)
        hitbox.right = hitbox.left + planeWidht
    }

    fun updatePlane(){
        if (planeX <= 10){
            planeX = 10f
            speed = 0
        }else if(planeX >= ScreenLimitX-120){
            planeX = ScreenLimitX - 120f
            speed = 0
        }else {
            planeX += speed
            planeY += speed
        }

        println("$ScreenLimitX y $ScreenLimitY")
        println("PLANE X : $planeX PLANE Y: $planeY ")

        if (planeY <= 0){
            planeY = 0f
            speed = 0
        }else if(planeY >= ScreenLimitY-planeHeight){
            planeY = ScreenLimitY - ((planeHeight))
            speed = 0
        }else {
            planeX += speed
            planeY += speed
        }

        hitbox.top = planeY - (planeHeight/2)
        hitbox.bottom = hitbox.top + planeHeight
        hitbox.left = planeX - (planeWidht/2)
        hitbox.right = hitbox.left + planeWidht

    }
}