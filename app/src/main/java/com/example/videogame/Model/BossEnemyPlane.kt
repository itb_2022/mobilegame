package com.example.videogame.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.view.View
import com.example.videogame.R

class BossEnemyPlane (context: Context, x: Int, y:Int, size: Point): View(context) {

    var bossEnemyPlane: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.redcircle)
    val bossEnemyPlaneWidht = x / 8f
    val bossEnemyPlaneHeight = y / 8f
    var bossEnemyPlaneX = 0f
    var bossEnemyPlaneY = 0f
    var speed = 0


    init {
        bossEnemyPlane = Bitmap.createScaledBitmap(bossEnemyPlane, bossEnemyPlaneWidht.toInt(), bossEnemyPlaneHeight.toInt(),false)

        bossEnemyPlaneX = size.x.toFloat()
        bossEnemyPlaneY = size.y/2f
    }

    fun updateBossEnemyPlane(){
        bossEnemyPlaneX += speed
        bossEnemyPlaneY += speed
    }
}