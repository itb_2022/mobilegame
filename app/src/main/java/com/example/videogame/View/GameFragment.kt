package com.example.videogame.View

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.GradientDrawable
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.Button
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.ColorInt
import androidx.core.view.marginRight
import com.example.videogame.Model.GameView

class GameFragment : Fragment() {

    lateinit var gameView: GameView
    lateinit var fireButton: Button

    /*
    FIRST ONCREATEVIEW CODE
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val display = requireActivity().windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        gameView = GameView(requireContext(), size)
        return gameView
    }
     */

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val game: FrameLayout = FrameLayout(requireContext())
        val gameButtons: RelativeLayout = RelativeLayout(requireContext())
        fireButton = Button(requireContext());

        val display = requireActivity().windowManager.defaultDisplay
        val size = Point()
        var counter = 0
        display.getSize(size)

        gameView = GameView(requireContext(), size, counter)

        fireButton.setText("Shoot")

        val radius = 16f
        val shapeDrawable = GradientDrawable()
        shapeDrawable.cornerRadius = radius

        shapeDrawable.setColor(Color.MAGENTA)
        fireButton.background = shapeDrawable

        val b1 = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            RelativeLayout.LayoutParams.FILL_PARENT
        )

        gameButtons.setLayoutParams(params)
        gameButtons.addView(fireButton)

        b1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE)
        b1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)

        fireButton.setLayoutParams(b1)

        game.addView(gameView)
        game.addView(gameButtons)

        return game
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fireButton.setOnClickListener {
            gameView.shotAction = true
        }
    }


//    override fun onClick(v: View?) {
//        when (v?.id) {
//            binding.startButton -> {
//                gameView?.startGame()
//            }
//            binding.pauseButton -> {
//                gameView?.pauseGame()
//            }
//            binding.resumeButton -> {
//                gameView?.resumeGame()
//            }
//        }
//    }

}